import flavio, time
from flavio.io import flha
import numpy as np


# Start timer 
starttime = time.time()

nsteps=30

#DeltaM_d_Exp=3.3321E-13
#err_exp  = 0.012502E-13
#DeltaM_d_SM = 3.6691011878452789e-13

data=np.zeros((nsteps,2))
for j in range(0,nsteps) :

    WC=flha.read_wilson('SUSY_RPV_mass/Output_DeltaM_'+str(j)+'.spc')
    DeltaM_d=flavio.np_prediction('DeltaM_d',WC)
   # err_theo = abs((DeltaM_d- DeltaM_d_SM)*0.3)+abs(0.15*(DeltaM_d_SM)) 
  #  err_tot  = np.sqrt(err_theo**2+err_exp**2)
    data[j,0]=np.genfromtxt('SUSY_RPV_mass/Output_DeltaM_'+str(j)+'.spc',skip_header=379,max_rows=1,usecols=1) #read sneutrino_1 mass
   # data[nsteps*j+k,1]=np.genfromtxt('SUSY_RPV/Output_DeltaM_d_'+str(j)+'_'+str(k)+'.spc',skip_header=132,max_rows=1,usecols=3) #read lambda'_113
   # data[nsteps*j+k,1]=data[nsteps*j+k,1]*data[nsteps*j+k,1] #square lambda'_113
   # data[nsteps*j+k,2]=err_tot
    data[j,1]=DeltaM_d
   # data[nsteps*j+k,4]=np.absolute(DeltaM_d - DeltaM_d_Exp)/err_tot


np.savetxt('SUSY_RPV_mass/data',data)


# Print time used by script
print("--- %s seconds ---" % round((time.time() - starttime),2))
