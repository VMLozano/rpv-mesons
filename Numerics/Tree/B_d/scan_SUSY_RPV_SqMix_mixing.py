#!/usr/bin/env python
import os, sys, subprocess, glob, time, random
import numpy as np

# Start timer 
starttime = time.time()

# Set Home director for different computers
from os.path import expanduser
home = expanduser("~")+'/'


# Set working directory and location of executables
input_directory = '/home/wzeren/Desktop/Research/HEP_Softwares/SPheno-4.0.3/bin'
spheno_exec_directory = '/home/wzeren/Desktop/Research/HEP_Softwares/SPheno-4.0.3/bin/SPhenoMSSMTriRpV/SPhenoMSSMTriRpV_full'


#higgsbounds_allowed = []
#higgsbounds_obsratio = []

node_folder ='/home/wzeren/Desktop/Research/SUSY_RPV/rpv-mesons/Numerics/Tree/B_d/SUSY_RPV_SqMix_mixing'
node_dir = node_folder+'/'

input_file_name = "LesHouches.in.MSSMTriRpV_low"

def WriteSPhenoInput(M1input, M2input, M3input, Muinput, TanBeta, MAinput, MSfermioninput,RPVinput,squarkmixinput) :
    with open(node_dir+input_file_name, "w+") as file :
      
	#-----------------------------------------------
      file.write("Block MODSEL      #  \n");
      file.write("1 0               #  1/0: High/low scale input \n");	
      file.write("2 1              # Boundary Condition  \n");
      file.write("6 1               # Generation Mixing \n");
      file.write("12 1000.               # Renormalization scale \n");
      file.write("Block SMINPUTS    # Standard Model inputs \n");
      file.write("2 1.166370E-05    # G_F,Fermi constant \n");
      file.write("3 1.187000E-01    # alpha_s(MZ) SM MSbar \n");
      file.write("4 9.118870E+01    # Z-boson pole mass \n");
      file.write("5 4.180000E+00    # m_b(mb) SM MSbar \n");
      file.write("6 1.735000E+02    # m_top(pole) \n");
      file.write("7 1.776690E+00    # m_tau(pole) \n");
      file.write("Block MINPAR      # Input parameters \n");
      file.write("Block EXTPAR      # Input parameters \n");
      file.write(" 1   "+repr(M1input)+"    # M1input \n");
      file.write(" 2   "+repr(M2input)+"    # M2input \n");
      file.write(" 3   "+repr(M3input)+"    # M3input \n");
      file.write(" 23   "+repr(Muinput)+"    # Muinput \n");
      file.write(" 25   "+repr(TanBeta)+"    # TanBeta \n");
      file.write(" 26   "+repr(MAinput)+"    # MAinput \n");
      file.write("Block SPhenoInput   # SPheno specific input \n");
      file.write("  1 -1              # error level \n");
      file.write("  2  0              # SPA conventions \n");
      file.write("  7  0              # Skip 2-loop Higgs corrections \n");
      file.write("  8  3              # Method used for two-loop calculation \n");
      file.write("  9  1              # Gaugeless limit used at two-loop \n");
      file.write(" 10  0              # safe-mode used at two-loop \n");
      file.write(" 11 0               # calculate branching ratios \n");
      file.write(" 13 0               # 3-Body decays: none (0), fermion (1), scalar (2), both (3) \n");
      file.write(" 14 0               # Run couplings to scale of decaying particle \n");
      file.write(" 12 1.000E-04       # write only branching ratios larger than this value \n");
      file.write(" 15 1.000E-30       # write only decay if width larger than this value \n");
      file.write(" 31 -1              # fixed GUT scale (-1: dynamical GUT scale) \n");
      file.write(" 32 0               # Strict unification \n");
      file.write(" 34 1.000E-04       # Precision of mass calculation \n");
      file.write(" 35 40              # Maximal number of iterations\n");
      file.write(" 36 5               # Minimal number of iterations before discarding points\n");
      file.write(" 37 1               # Set Yukawa scheme  \n");
      file.write(" 38 2               # 1- or 2-Loop RGEs \n");
      file.write(" 50 1               # Majorana phases: use only positive masses (put 0 to use file with CalcHep/Micromegas!) \n");
      file.write(" 51 0               # Write Output in CKM basis \n");
      file.write(" 52 0               # Write spectrum in case of tachyonic states \n");
      file.write(" 55 1               # Calculate loop corrected masses \n");
      file.write(" 57 1               # Calculate low energy constraints \n");
      file.write(" 65 1               # Solution tadpole equation \n");
      file.write(" 66 1               # Two-Scale Matching \n");
      file.write(" 67 1               # effective Higgs mass calculation \n");
      file.write(" 75 1               # Write WHIZARD files \n");
      file.write(" 76 1               # Write HiggsBounds file   \n");
      file.write(" 77 0               # Output for MicrOmegas (running masses for light quarks; real mixing matrices)   \n");
      file.write(" 78 0               # Output for MadGraph (writes also vanishing blocks)      \n");
      file.write(" 86 0.              # Maximal width to be counted as invisible in Higgs decays; -1: only LSP \n");
      file.write("510 0.              # Write tree level values for tadpole solutions \n");
      file.write("515 0               # Write parameter values at GUT scale \n");
      file.write("520 1.              # Write effective Higgs couplings (HiggsBounds blocks): put 0 to use file with MadGraph!  \n");
      file.write("521 1.              # Diphoton/Digluon widths including higher order \n");
      file.write("525 0.              # Write loop contributions to diphoton decay of Higgs \n");
      file.write("530 1.              # Write Blocks for Vevacious \n");
      file.write("Block DECAYOPTIONS   # Options to turn on/off specific decays \n");
      file.write("1    1     # Calc 3-Body decays of Glu \n");
      file.write("2    1     # Calc 3-Body decays of Chi \n");
      file.write("3    1     # Calc 3-Body decays of Cha \n");
      file.write("4    1     # Calc 3-Body decays of Sd  \n");
      file.write("5    1     # Calc 3-Body decays of Su  \n");
      file.write("6    1     # Calc 3-Body decays of Se  \n");
      file.write("7    1     # Calc 3-Body decays of Sv  \n");
      file.write("Block RVLAMLLEIN    #  \n");
      file.write("1 1 1   0.000000E+00         # L1(1,1,1)\n");
      file.write("1 1 2   0.000000E+00         # L1(1,1,2)\n");
      file.write("1 1 3   0.000000E+00         # L1(1,1,3)\n");
      file.write("1 2 1   0.000000E+00         # L1(1,2,1)\n");
      file.write("1 2 2   0.000000E+00         # L1(1,2,2)\n");
      file.write("1 2 3   0.000000E+00         # L1(1,2,3)\n");
      file.write("1 3 1   0.000000E+00         # L1(1,3,1)\n");
      file.write("1 3 2   0.000000E+00         # L1(1,3,2)\n");
      file.write("1 3 3   0.000000E+00         # L1(1,3,3)\n");
      file.write("2 1 1   0.000000E+00         # L1(2,1,1)\n");
      file.write("2 1 2   0.000000E+00         # L1(2,1,2)\n");
      file.write("2 1 3   0.000000E+00         # L1(2,1,3)\n");
      file.write("2 2 1   0.000000E+00         # L1(2,2,1)\n");
      file.write("2 2 2   0.000000E+00         # L1(2,2,2)\n");
      file.write("2 2 3   0.000000E+00         # L1(2,2,3)\n");
      file.write("2 3 1   0.000000E+00         # L1(2,3,1)\n");
      file.write("2 3 2   0.000000E+00         # L1(2,3,2)\n");
      file.write("2 3 3   0.000000E+00         # L1(2,3,3)\n");
      file.write("3 1 1   0.000000E+00         # L1(3,1,1)\n");
      file.write("3 1 2   0.000000E+00         # L1(3,1,2)\n");
      file.write("3 1 3   0.000000E+00         # L1(3,1,3)\n");
      file.write("3 2 1   0.000000E+00         # L1(3,2,1)\n");
      file.write("3 2 2   0.000000E+00         # L1(3,2,2)\n");
      file.write("3 2 3   0.000000E+00         # L1(3,2,3)\n");
      file.write("3 3 1   0.000000E+00         # L1(3,3,1)\n");
      file.write("3 3 2   0.000000E+00         # L1(3,3,2)\n");
      file.write("3 3 3   0.000000E+00         # L1(3,3,3)\n");
      file.write("Block RVLAMLQDIN    #  \n");
      file.write("1 1 1   0.000000E+00         # L2(1,1,1)\n");
      file.write("1 1 2   0.000000E+00         # L2(1,1,2)\n");
      file.write("1 1 3   "+repr(RPVinput)+"         # L2(1,1,3)\n");
      file.write("1 2 1   0.000000E+00         # L2(1,2,1)\n");
      file.write("1 2 2   0.000000E+00         # L2(1,2,2)\n");
      file.write("1 2 3   0.000000E+00         # L2(1,2,3)\n");
      file.write("1 3 1   "+repr(RPVinput)+"         # L2(1,3,1)\n");
      file.write("1 3 2   0.000000E+00         # L2(1,3,2)\n");
      file.write("1 3 3   0.000000E+00         # L2(1,3,3)\n");
      file.write("2 1 1   0.000000E+00         # L2(2,1,1)\n");
      file.write("2 1 2   0.000000E+00         # L2(2,1,2)\n");
      file.write("2 1 3   0.000000E+00         # L2(2,1,3)\n");
      file.write("2 2 1   0.000000E+00         # L2(2,2,1)\n");
      file.write("2 2 2   0.000000E+00         # L2(2,2,2)\n");
      file.write("2 2 3   0.000000E+00         # L2(2,2,3)\n");
      file.write("2 3 1   0.000000E+00         # L2(2,3,1)\n");
      file.write("2 3 2   0.000000E+00         # L2(2,3,2)\n");
      file.write("2 3 3   0.000000E+00         # L2(2,3,3)\n");
      file.write("3 1 1   0.000000E+00         # L2(3,1,1)\n");
      file.write("3 1 2   0.000000E+00         # L2(3,1,2)\n");
      file.write("3 1 3   0.000000E+00         # L2(3,1,3)\n");
      file.write("3 2 1   0.000000E+00         # L2(3,2,1)\n");
      file.write("3 2 2   0.000000E+00         # L2(3,2,2)\n");
      file.write("3 2 3   0.000000E+00         # L2(3,2,3)\n");
      file.write("3 3 1   0.000000E+00         # L2(3,3,1)\n");
      file.write("3 3 2   0.000000E+00         # L2(3,3,2)\n");
      file.write("3 3 3   0.000000E+00         # L2(3,3,3)\n");
      file.write("Block RVLAMUDDIN    # \n");
      file.write("1 1 1   0.000000E+00         # L3(1,1,1)\n");
      file.write("1 1 2   0.000000E+00         # L3(1,1,2)\n");
      file.write("1 1 3   0.000000E+00         # L3(1,1,3)\n");
      file.write("1 2 1   0.000000E+00         # L3(1,2,1)\n");
      file.write("1 2 2   0.000000E+00         # L3(1,2,2)\n");
      file.write("1 2 3   0.000000E+00         # L3(1,2,3)\n");
      file.write("1 3 1   0.000000E+00         # L3(1,3,1)\n");
      file.write("1 3 2   0.000000E+00         # L3(1,3,2)\n");
      file.write("1 3 3   0.000000E+00         # L3(1,3,3)\n");
      file.write("2 1 1   0.000000E+00         # L3(2,1,1)\n");
      file.write("2 1 2   0.000000E+00         # L3(2,1,2)\n");
      file.write("2 1 3   0.000000E+00         # L3(2,1,3)\n");
      file.write("2 2 1   0.000000E+00         # L3(2,2,1)\n");
      file.write("2 2 2   0.000000E+00         # L3(2,2,2)\n");
      file.write("2 2 3   0.000000E+00         # L3(2,2,3)\n");
      file.write("2 3 1   0.000000E+00         # L3(2,3,1)\n");
      file.write("2 3 2   0.000000E+00         # L3(2,3,2)\n");
      file.write("2 3 3   0.000000E+00         # L3(2,3,3)\n");
      file.write("3 1 1   0.000000E+00         # L3(3,1,1)\n");
      file.write("3 1 2   0.000000E+00         # L3(3,1,2)\n");
      file.write("3 1 3   0.000000E+00         # L3(3,1,3)\n");
      file.write("3 2 1   0.000000E+00         # L3(3,2,1)\n");
      file.write("3 2 2   0.000000E+00         # L3(3,2,2)\n");
      file.write("3 2 3   0.000000E+00         # L3(3,2,3)\n");
      file.write("3 3 1   0.000000E+00         # L3(3,3,1)\n");
      file.write("3 3 2   0.000000E+00         # L3(3,3,2)\n");
      file.write("3 3 3   0.000000E+00         # L3(3,3,3)\n");
      file.write("Block MSD2IN    #  \n");
      file.write("1 1   3.500000E+6         # md2(1,1)\n");
      file.write("1 2   0.000000E+00         # md2(1,2)\n");
      file.write("1 3   "+repr(squarkmixinput)+"         # md2(1,3)\n");
      file.write("2 1   0.000000E+00         # md2(2,1)\n");
      file.write("2 2   3.500000E+6         # md2(2,2)\n");
      file.write("2 3   0.000000E+00         # md2(2,3)\n");
      file.write("3 1   "+repr(squarkmixinput)+"         # md2(3,1)\n");
      file.write("3 2   0.000000E+00         # md2(3,2)\n");
      file.write("3 3   3.500000E+6         # md2(3,3)\n");
      file.write("Block MSE2IN    #  \n");
      file.write("1 1   "+repr(MSfermioninput)+"        # md2(1,1)\n");
      file.write("1 2   0.000000E+00         # md2(1,2)\n");
      file.write("1 3   0.000000E+00         # md2(1,3)\n");
      file.write("2 1   0.000000E+00         # md2(2,1)\n");
      file.write("2 2   "+repr(MSfermioninput)+"         # md2(2,2)\n");
      file.write("2 3   0.000000E+00         # md2(2,3)\n");
      file.write("3 1   0.000000E+00         # md2(3,1)\n");
      file.write("3 2   0.000000E+00         # md2(3,2)\n");
      file.write("3 3   "+repr(MSfermioninput)+"         # md2(3,3)\n");
      file.write("Block MSL2IN    #  \n");
      file.write("1 1   "+repr(MSfermioninput)+"         # md2(1,1)\n");
      file.write("1 2   0.000000E+00         # md2(1,2)\n");
      file.write("1 3   0.000000E+00         # md2(1,3)\n");
      file.write("2 1   0.000000E+00         # md2(2,1)\n");
      file.write("2 2   "+repr(MSfermioninput)+"         # md2(2,2)\n");
      file.write("2 3   0.000000E+00         # md2(2,3)\n");
      file.write("3 1   0.000000E+00         # md2(3,1)\n");
      file.write("3 2   0.000000E+00         # md2(3,2)\n");
      file.write("3 3   "+repr(MSfermioninput)+"         # md2(3,3)\n");
      file.write("Block MSQ2IN    #  \n");
      file.write("1 1   3.500000E+6         # md2(1,1)\n");
      file.write("1 2   0.000000E+00         # md2(1,2)\n");
      file.write("1 3   "+repr(squarkmixinput)+"         # md2(1,3)\n");
      file.write("2 1   0.000000E+00         # md2(2,1)\n");
      file.write("2 2   3.500000E+6         # md2(2,2)\n");
      file.write("2 3   0.000000E+00         # md2(2,3)\n");
      file.write("3 1   "+repr(squarkmixinput)+"         # md2(3,1)\n");
      file.write("3 2   0.000000E+00         # md2(3,2)\n");
      file.write("3 3   3.500000E+6         # md2(3,3)\n");
      file.write("Block MSU2IN    #  \n");
      file.write("1 1   3.500000E+6         # md2(1,1)\n");
      file.write("1 2   0.000000E+00         # md2(1,2)\n");
      file.write("1 3   0.000000E+00         # md2(1,3)\n");
      file.write("2 1   0.000000E+00         # md2(2,1)\n");
      file.write("2 2   3.500000E+6         # md2(2,2)\n");
      file.write("2 3   0.000000E+00         # md2(2,3)\n");
      file.write("3 1   0.000000E+00         # md2(3,1)\n");
      file.write("3 2   0.000000E+00         # md2(3,2)\n");
      file.write("3 3   3.500000E+6         # md2(3,3)\n");
      file.write("Block RVTLLEIN    #  \n");
      file.write("1 1 1   0.000000E+00         # T1(1,1,1)\n");
      file.write("1 1 2   0.000000E+00         # T1(1,1,2)\n");
      file.write("1 1 3   0.000000E+00         # T1(1,1,3)\n");
      file.write("1 2 1   0.000000E+00         # T1(1,2,1)\n");
      file.write("1 2 2   0.000000E+00         # T1(1,2,2)\n");
      file.write("1 2 3   0.000000E+00         # T1(1,2,3)\n");
      file.write("1 3 1   0.000000E+00         # T1(1,3,1)\n");
      file.write("1 3 2   0.000000E+00         # T1(1,3,2)\n");
      file.write("1 3 3   0.000000E+00         # T1(1,3,3)\n");
      file.write("2 1 1   0.000000E+00         # T1(2,1,1)\n");
      file.write("2 1 2   0.000000E+00         # T1(2,1,2)\n");
      file.write("2 1 3   0.000000E+00         # T1(2,1,3)\n");
      file.write("2 2 1   0.000000E+00         # T1(2,2,1)\n");
      file.write("2 2 2   0.000000E+00         # T1(2,2,2)\n");
      file.write("2 2 3   0.000000E+00         # T1(2,2,3)\n");
      file.write("2 3 1   0.000000E+00         # T1(2,3,1)\n");
      file.write("2 3 2   0.000000E+00         # T1(2,3,2)\n");
      file.write("2 3 3   0.000000E+00         # T1(2,3,3)\n");
      file.write("3 1 1   0.000000E+00         # T1(3,1,1)\n");
      file.write("3 1 2   0.000000E+00         # T1(3,1,2)\n");
      file.write("3 1 3   0.000000E+00         # T1(3,1,3)\n");
      file.write("3 2 1   0.000000E+00         # T1(3,2,1)\n");
      file.write("3 2 2   0.000000E+00         # T1(3,2,2)\n");
      file.write("3 2 3   0.000000E+00         # T1(3,2,3)\n");
      file.write("3 3 1   0.000000E+00         # T1(3,3,1)\n");
      file.write("3 3 2   0.000000E+00         # T1(3,3,2)\n");
      file.write("3 3 3   0.000000E+00         # T1(3,3,3)\n");
      file.write("Block RVTLQDIN    #  \n");
      file.write("1 1 1   0.000000E+00         # T2(1,1,1)\n");
      file.write("1 1 2   0.000000E+00         # T2(1,1,2)\n");
      file.write("1 1 3   0.000000E+00         # T2(1,1,3)\n");
      file.write("1 2 1   0.000000E+00         # T2(1,2,1)\n");
      file.write("1 2 2   0.000000E+00         # T2(1,2,2)\n");
      file.write("1 2 3   0.000000E+00         # T2(1,2,3)\n");
      file.write("1 3 1   0.000000E+00         # T2(1,3,1)\n");
      file.write("1 3 2   0.000000E+00         # T2(1,3,2)\n");
      file.write("1 3 3   0.000000E+00         # T2(1,3,3)\n");
      file.write("2 1 1   0.000000E+00         # T2(2,1,1)\n");
      file.write("2 1 2   0.000000E+00         # T2(2,1,2)\n");
      file.write("2 1 3   0.000000E+00         # T2(2,1,3)\n");
      file.write("2 2 1   0.000000E+00         # T2(2,2,1)\n");
      file.write("2 2 2   0.000000E+00         # T2(2,2,2)\n");
      file.write("2 2 3   0.000000E+00         # T2(2,2,3)\n");
      file.write("2 3 1   0.000000E+00         # T2(2,3,1)\n");
      file.write("2 3 2   0.000000E+00         # T2(2,3,2)\n");
      file.write("2 3 3   0.000000E+00         # T2(2,3,3)\n");
      file.write("3 1 1   0.000000E+00         # T2(3,1,1)\n");
      file.write("3 1 2   0.000000E+00         # T2(3,1,2)\n");
      file.write("3 1 3   0.000000E+00         # T2(3,1,3)\n");
      file.write("3 2 1   0.000000E+00         # T2(3,2,1)\n");
      file.write("3 2 2   0.000000E+00         # T2(3,2,2)\n");
      file.write("3 2 3   0.000000E+00         # T2(3,2,3)\n");
      file.write("3 3 1   0.000000E+00         # T2(3,3,1)\n");
      file.write("3 3 2   0.000000E+00         # T2(3,3,2)\n");
      file.write("3 3 3   0.000000E+00         # T2(3,3,3)\n");
      file.write("Block RVTUDDIN    #  \n");
      file.write("1 1 1   0.000000E+00         # T3(1,1,1)\n");
      file.write("1 1 2   0.000000E+00         # T3(1,1,2)\n");
      file.write("1 1 3   0.000000E+00         # T3(1,1,3)\n");
      file.write("1 2 1   0.000000E+00         # T3(1,2,1)\n");
      file.write("1 2 2   0.000000E+00         # T3(1,2,2)\n");
      file.write("1 2 3   0.000000E+00         # T3(1,2,3)\n");
      file.write("1 3 1   0.000000E+00         # T3(1,3,1)\n");
      file.write("1 3 2   0.000000E+00         # T3(1,3,2)\n");
      file.write("1 3 3   0.000000E+00         # T3(1,3,3)\n");
      file.write("2 1 1   0.000000E+00         # T3(2,1,1)\n");
      file.write("2 1 2   0.000000E+00         # T3(2,1,2)\n");
      file.write("2 1 3   0.000000E+00         # T3(2,1,3)\n");
      file.write("2 2 1   0.000000E+00         # T3(2,2,1)\n");
      file.write("2 2 2   0.000000E+00         # T3(2,2,2)\n");
      file.write("2 2 3   0.000000E+00         # T3(2,2,3)\n");
      file.write("2 3 1   0.000000E+00         # T3(2,3,1)\n");
      file.write("2 3 2   0.000000E+00         # T3(2,3,2)\n");
      file.write("2 3 3   0.000000E+00         # T3(2,3,3)\n");
      file.write("3 1 1   0.000000E+00         # T3(3,1,1)\n");
      file.write("3 1 2   0.000000E+00         # T3(3,1,2)\n");
      file.write("3 1 3   0.000000E+00         # T3(3,1,3)\n");
      file.write("3 2 1   0.000000E+00         # T3(3,2,1)\n");
      file.write("3 2 2   0.000000E+00         # T3(3,2,2)\n");
      file.write("3 2 3   0.000000E+00         # T3(3,2,3)\n");
      file.write("3 3 1   0.000000E+00         # T3(3,3,1)\n");
      file.write("3 3 2   0.000000E+00         # T3(3,3,2)\n");
      file.write("3 3 3   0.000000E+00         # T3(3,3,3)\n");
      file.write("Block TDIN    #  \n");
      file.write("1 1   0.000000E+00         # Td(1,1)\n");
      file.write("1 2   0.000000E+00         # Td(1,2)\n");
      file.write("1 3   0.000000E+00         # Td(1,3)\n");
      file.write("2 1   0.000000E+00         # Td(2,1)\n");
      file.write("2 2   0.000000E+00         # Td(2,2)\n");
      file.write("2 3   0.000000E+00         # Td(2,3)\n");
      file.write("3 1   0.000000E+00         # Td(3,1)\n");
      file.write("3 2   0.000000E+00         # Td(3,2)\n");
      file.write("3 3   0.000000E+00         # Td(3,3)\n");
      file.write("Block TEIN    #  \n");
      file.write("1 1   0.000000E+00         # Te(1,1)\n");
      file.write("1 2   0.000000E+00         # Te(1,2)\n");
      file.write("1 3   0.000000E+00         # Te(1,3)\n");
      file.write("2 1   0.000000E+00         # Te(2,1)\n");
      file.write("2 2   0.000000E+00         # Te(2,2)\n");
      file.write("2 3   0.000000E+00         # Te(2,3)\n");
      file.write("3 1   0.000000E+00         # Te(3,1)\n");
      file.write("3 2   0.000000E+00         # Te(3,2)\n");
      file.write("3 3   0.000000E+00         # Te(3,3)\n");
      file.write("Block TUIN    #  \n");
      file.write("1 1   0.000000E+00         # Tu(1,1)\n");
      file.write("1 2   0.000000E+00         # Tu(1,2)\n");
      file.write("1 3   0.000000E+00         # Tu(1,3)\n");
      file.write("2 1   0.000000E+00         # Tu(2,1)\n");
      file.write("2 2   0.000000E+00         # Tu(2,2)\n");
      file.write("2 3   0.000000E+00         # Tu(2,3)\n");
      file.write("3 1   0.000000E+00         # Tu(3,1)\n");
      file.write("3 2   0.000000E+00         # Tu(3,2)\n");
      file.write("3 3   3.000000E+3         # Tu(3,3)\n");
      file.close()

print_SPheno_Output = True
	
def CallSPheno(input, output) :
	FNULL = open(os.devnull, 'w')
	if print_SPheno_Output == True :
		subprocess.call([spheno_exec_directory, input, output])
	if print_SPheno_Output == False :
		subprocess.call([spheno_exec_directory, input, output],stdout=FNULL)
    
#def CallHiggsBounds(input) :
    #subprocess.call([higgsbounds_exec_directory, 'LandH', 'effC', '3', '1', input+'_'])
    
#def CallHiggsSignals(input) :
    #subprocess.call([higgssignals_exec_directory, 'latestresults', 'peak', '2', 'effC', '3', '1', input+'_'])

nsteps = 30

MA_in = 1200
M1_in=  500
M2_in=  500
M3_in=  2000
Mu_in = 600
TanBeta_in=10

MSfermion_in=1e6
RPV_in=0


Squarkmix_vals = np.linspace(1e4,1e6,nsteps)
#np.concatenate([np.linspace(1E4,1E5,nsteps/3*1),np.linspace(2E5,6E6,nsteps/3*2)])

os.mkdir(node_folder)
os.chdir(node_dir+'/')


for j in range(0,len(Squarkmix_vals)) :
    
    Squarkmix_in = Squarkmix_vals[j]
    print('j-index: ',j)
    # Write SPheno input for the above point
    WriteSPhenoInput(M1_in, M2_in, M3_in, Mu_in, TanBeta_in, MA_in, MSfermion_in,RPV_in,Squarkmix_in)
    
    # Call SPheno
    Output_Name = 'Output_DeltaM_'+str(j)+'.spc'
    # print Output_Name
    CallSPheno(input_file_name, Output_Name)
    
    # Call HiggsBounds if .spc file is produced
    #if os.path.isfile(Output_Name+'.spc') :
	#CallHiggsBounds(Output_Name)
	#CallHiggsSignals(Output_Name)
	


# Print time used by script
print("--- %s seconds ---" % round((time.time() - starttime),2))
